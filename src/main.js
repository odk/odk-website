// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import messages from './locale/messages';
import VueResource from 'vue-resource';
import Vuelidate from 'vuelidate'
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import VueI18n from 'vue-i18n';
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
export const eventBus = new Vue();

Vue.config.productionTip = false;


Vue.use(Vuelidate)
Vue.use(Buefy)
Vue.use(VueResource)
Vue.use(VueI18n)
Vue.use(VueAwesomeSwiper, /* { default global options } */ )
AOS.init()





const i18n = new VueI18n({
  locale: 'nl', // set locale
  fallbackLocale: 'nl',
  messages, // set locale messages
})



new Vue({
  router,
  i18n,
  render: h => h(App),
}).$mount('#app')