export const homePageText = {
    i18n: {
        messages: {
            en: {
                subtitle: "Scanning objects for a cleaner city",
                startButton: "Get started",
                complaints: "Less garbage on the streets",
                manyComplaints:
                    "ODK is an image recognition tool for municipalities to spot misplaced garbage in the streets and act on it quickly, even before citizens notice.",
                convert: "Convert garbage into resources",
                convertResources:
                    "The garbage on our streets can have a lot of value when it is repurposed. ODK aims to take a step forward into circularity, making it easier to give garbage a new life.",
                setupHeader: "Quick setup & enjoy your ride",
                setupText:
                    "After installing the tool, ODK starts scanning every time you open the app and start driving. While driving you will detect garbage on the streets.",
                setupButton: "Check how to setup",
                pwaButton: "Web App",
                launchNow: "LAUNCH NOW AS",
                processHeader: "One garbage bag, one report",
                processText:
                    "Our model is designed to recognize different types of objects. Once you start scanning, ODK will automatically start mapping the objects. Connected services can now see where garbage is detected, so they can pick it up!",
                detect: "1. Detect",
                report: "2. Report",
                collect: "3. Collect",
                keepClean: "4. Clean streets",
                currentheader: "Currently recognized objects",
                items: "4 objects",
                cardboard: "Cardboard",
                garbageBag: "Garbage bags",
                garbageBins: "Underground containers",
                furniture: "Furniture",
                appliances: "Appliances",
                christmasTree: "Christmas trees",
                Mattresses: "Mattresses",
                wood: "Wood",
                glass: "Glass",
                textiles: "Textiles",
                comingSoon: "coming soon",
                join: "Join us",
                helpEachOther: "Together we’ll keep our city clean!",
                titleOptionOne: "Use ODK in Amsterdam",
                optionOne: "I want to use ODK in municipality vehicles",
                titleOptionTwo: "Use ODK in my city",
                optionTwo: "I want to know how my municipality can join ODK",
                titleOptionThree: "Help labelling images",
                optionThree: "I want to help labelling images to improve accuracy",
                titleOptionFour: "Detect other objects",
                optionFour: "I want to start detecting objects other than garbage",
                bottomTextHeader: "Perfection is a process",
                bottomText:
                    "ODK is continuously updating for a more accurate algorithm. The data collected is displayed in a dashboard accessible by municipalities to better plan and execute city maintenance.",
                bottomTextStatusOne: { number: "10", text: "Hours streamed" },
                bottomTextStatusTwo: { number: "± 20.000", text: "Objects detected" },
                bottomTextStatusThree: { number: "4", text: "ODK updates" },
                bottomButton: "Next steps",
                roadmapDate: "December 2019",
                roadmapTitle: "Website launch",
                roadmapBody:
                    "The website is part of ODK communication strategy. It is aimed to help citizens understand the need and benefits of image recognition in public spaces and for municipalities to join and collaborate new developments."
            },
            nl: {
                subtitle: "Afval spotten voor een schonere stad",
                startButton: "Doe mee",
                complaints: "Minder afval op straat",
                manyComplaints:
                    "ODK is een beeldherkenningstool voor gemeenten om verkeerd geplaatst afval op straat op te sporen en daar snel naar te handelen, nog voordat bewoners er last van hebben.",
                convert: "Geef afval nieuwe waarde",
                convertResources:
                    "Hoe minder afval, hoe beter. ODK draagt bij aan een circulaire stad en maakt het mogelijk om sommige spullen te hergebruiken.",
                setupHeader: "Installeren, rijden en klaar",
                setupText:
                    "Nadat je de app hebt geïnstalleerd, start ODK automatisch met scannen zodra je de app hebt geopend en begint te rijden. Tijdens het rijden wordt het afval gedetecteerd.",
                setupButton: "Bekijk installatie",
                pwaButton: "Web App",
                launchNow: "LAUNCH NOW AS",
                processHeader: "Realtime meldingen van afval",
                processText:
                    "Een 'machine learning model' is getraind om verschillende soorten objecten te herkennen. Wanneer je begint met rijden worden deze automische geplot op een kaart en zijn zo zichtbaar voor relevante diensten.",
                detect: "1. Scannen",
                report: "2. Melden",
                collect: "3. Ophalen",
                keepClean: "4. Schone stad",
                currentheader: "Dit herkent ODK nu",
                items: "4 objecten",
                cardboard: "Karton",
                garbageBag: "Afvalzakken",
                garbageBins: "Afvalcontainer",
                furniture: "Meubels",
                appliances: "Witgoed & Elektronica",
                christmasTree: "Kerstbomen",
                Mattresses: "Matrassen",
                wood: "Hout",
                glass: "Glas",
                textiles: "Textiel",
                comingSoon: "Binnenkort",
                join: "Doe mee!",
                helpEachOther: "Samen werken we aan een schone stad!",
                titleOptionOne: "ODK in Amsterdam",
                optionOne:
                    "Ik wil ODK gebruiken in voertuigen van de gemeente Amsterdam",
                titleOptionTwo: "Gebruik ODK in andere steden",
                optionTwo: "Ik wil weten hoe mijn stad gebruik kan maken van ODK",
                titleOptionThree: "Help met afbeeldingen herkennen",
                optionThree:
                    "Ik wil helpen bij het labelen van afbeeldingen om de nauwkeurigheid te verbeteren",
                titleOptionFour: "Andere objecten detecteren",
                optionFour: "Ik wil andere dingen dan afval detecteren",
                bottomTextHeader: "Continu verbeteren",
                bottomText:
                    "We werken ODK voortdurend bij voor een nauwkeuriger algoritme. De verzamelde gegevens worden weergegeven in een dashboard dat toegankelijk is voor gemeenten. Hiermee kunnen gemeenten het schoonhouden van de stad beter plannen en voorspellen.",
                bottomTextStatusOne: { number: "10", text: "Uren gestreamd" },
                bottomTextStatusTwo: {
                    number: "± 20.000",
                    text: "Objecten gedetecteerd"
                },
                bottomTextStatusThree: { number: "4", text: "ODK updates" },
                bottomButton: "Volgende stappen",
                roadmapDate: "December 2019",
                roadmapTitle: "Website launch",
                roadmapBody:
                    "The website is part of ODK communication strategy. It is aimed to help citizens understand the need and benefits of image recognition in public spaces and for municipalities to join and collaborate new developments."
            }
        }
    }
}

export const setupPageText = {
    i18n: {
        messages: {
            en: {
                subTitle:
                    "After installing the tool, ODK starts scanning every time you open the app and start driving. While driving you will detect garbage on the streets",
                componentTitle: "Components",
                step: "Step"
            },
            nl: {
                subTitle:
                    "Na de installatie van de tool, begint ODK met scannen zodra het voertuig begint te rijden. Tijdens het rijden wordt het afval gedetecteerd",
                componentTitle: "Onderdelen",
                step: "Stap"
            }
        }
    }
}

export const setupComponents = {
    i18n: {
        messages: {
            en: {
                components: [{
                    component: "Mobile phone",
                    componentImage: "phone"
                },
                {
                    component: "Phone holder",
                    componentImage: "holder"
                },
                {
                    component: "Car charger",
                    componentImage: "charger"
                },
                {
                    component: "ODK App",
                    componentImage: "app"
                },
                {
                    component: "Sticker",
                    componentImage: "sticker"
                }],
            },
            nl: {
                components: [{
                    component: "Mobiele telefoon",
                    componentImage: "phone"
                },
                {
                    component: "Telefoonhouder",
                    componentImage: "holder"
                },
                {
                    component: "Autolader",
                    componentImage: "charger"
                },
                {
                    component: "ODK App",
                    componentImage: "app"
                },
                {
                    component: "Sticker",
                    componentImage: "sticker"
                }],
            }
        }
    }
}

export const setupSteps = {
    i18n: {
        messages: {
            en: {
                steps: [{
                    stepNumber: "1",
                    stepImage: "image_setup_step_1_cropped.jpg",
                    stepTitle: "Open ODK app",
                    stepTask1: "Navigate to a public version of the app ",
                    stepTask2: "On this website you can add the ODK app to the home screen of your device. Now you can access it like any other app.",
                    stepLink: "https://app.odk.cto-tech.amsterdam/"
                },
                {
                    stepNumber: "2",
                    stepImage: "gif_setup_step_2.gif",
                    stepTitle: "Place the phone holder",
                    stepTask1: "Attach the phone holder to the front window;",
                    stepTask2: "Place your phone in the holder.",
                    stepInfo: "Position the camera so it faces slightly down; Keep the phone out of your sight and action area to avoid interferences while driving."
                },
                {
                    stepNumber: "3",
                    stepImage: "gif_setup_step_3.gif",
                    stepTitle: "Plug to charger",
                    stepTask1: "Check if your car has a USB supported power outlet.",
                    stepTask2: "Connect the phone to the car charger."
                },
                {
                    stepNumber: "4",
                    stepImage: "image_setup_step_4_cropped-vague.jpg",
                    stepTitle: "Place the sticker",
                    stepTask1: "Add the sticker in a visible area, so it's clear what is being scanned by the vehicle.",
                    // stepTask2: "Print it here",
                    // stepLink: ""
                },
                {
                    stepNumber: "5",
                    stepImage: "gif_setup_step_5.gif",
                    stepTitle: "Start scanning",
                    stepTask1: "Open the App;",
                    stepTask2: "Tap ‘start’, and you’ll start scanning.",
                    stepInfo: "Another option is to select automatic mode. When using this mode, the app will start scanning when the vehicle starts driving. During your trip garbage will be detected and reported automatically."
                }]
            },
            nl: {
                steps: [{
                    stepNumber: "1",
                    stepImage: "image_setup_step_1_cropped.jpg",
                    stepTitle: "Open de ODK App",
                    stepTask1: "Navigeer naar een publieke versie van de app ",
                    stepTask2: "Op deze website voeg je de ODK app toe aan het startscherm van je telefoon. ODK werkt net als elke andere app.",
                    stepLink: "https://app.odk.cto-tech.amsterdam/"
                },
                {
                    stepNumber: "2",
                    stepImage: "gif_setup_step_2.gif",
                    stepTitle: "Plaats de telefoonhouder",
                    stepTask1: "Plaats de telefoonhouder op de voorruit;",
                    stepTask2: "Plaats de mobiele telefoon in de telefoonhouder.",
                    stepInfo: "Richt de camera een beetje naar beneden. Zorg ervoor dat de mobiele telefoon het zicht en het autorijden niet belemmert."
                },
                {
                    stepNumber: "3",
                    stepImage: "gif_setup_step_3.gif",
                    stepTitle: "Sluit de oplader aan",
                    stepTask1: "Check of het voertuig een USB-aansluiting heeft, zodat de telefoon tijdens het rijden opgeladen wordt.",
                    stepTask2: "Sluit de oplaadkabel aan op de mobiele telefoon."
                },
                {
                    stepNumber: "4",
                    stepImage: "image_setup_step_4_cropped-vague.jpg",
                    stepTitle: "Bevestig de sticker",
                    stepTask1: "Plak de sticker op een zichtbare plek, zodat mensen op straat weten wat het voertuig aan het scannen is.",
                    // stepTask2: "Print de sticker hier",
                    // stepLink: ""
                },
                {
                    stepNumber: "5",
                    stepImage: "gif_setup_step_5.gif",
                    stepTitle: "Begin met scannen",
                    stepTask1: "Open de App;",
                    stepTask2: "Druk op ‘start’ om te beginnen met scannen.",
                    stepInfo: "Of gebruik de automatische modus: Zodra het voertuig rijdt, begint het scannen vanzelf. Tijdens de rit wordt afval automatisch herkend en doorgegeven."
                }]
            }
        }
    }
}

export const participationPageText = {
    i18n: {
        messages: {
            en: {
                title: "Make your city future proof",
                subTitle:
                    "ODK is an open source platform for cities to help increase the efficiency of urban services",
                // Section 1
                sectionOneSmallHeader: "Amsterdam civil servants",
                sectionOneHeader: "Use ODK in Amsterdam",
                sectionOneText:
                    "More cars scanning garbage means better insight on the locations of garbage, which leads to cleaner streets. If you would like to know how to install the scanner in your vehicle, check out our setup manual on this website.",
                sectionOneButton: "Setup",
                // Section 2
                sectionTwoSmallHeader: "Local governments",
                sectionTwoHeader: "Use ODK in my city",
                sectionTwoText:
                    "If you want to start testing ODK in your city, you can start by deploying the code yourself. We work open source, so the code is always up to date. There might still be some bugs, don't be afraid to contribute.",
                sectionTwoButton: "GitLab",
                // Section 3
                sectionThreeSmallHeader: "Citizens, students and researchers",
                sectionThreeHeader: "Help Labeling",
                sectionThreeText:
                    "For accurate image recognition, we need to train the algorithm. Contribute by uploading street images with garbage and label them by respective category.",
                sectionThreeButton: "Labelling tool",
                // Section 4
                sectionFourSmallHeader: "Students, researchers and local governments",
                sectionFourHeader: "Detect other objects",
                sectionFourText:
                    "ODK currently recognizes a selected group of objects related to the waste department. If you want to detect another object, please do so! Start by deploying the code yourself via the GitLab page and use the labeling tool for training new objects.",
                sectionFourButtonOne: "Labelling tool",
                sectionFourButtonTwo: "GitLab"
            },
            nl: {
                title: "Werk mee aan de toekomst",
                subTitle:
                    "ODK is een open source platform voor steden om de dienstverlening van gemeenten te verbeteren",
                // Section 1
                sectionOneSmallHeader: "Amsterdamse ambtenaren",
                sectionOneHeader: "ODK in Amsterdam",
                sectionOneText:
                    "Hoe meer auto’s afval scannen, hoe meer informatie de gemeente verzamelt over afval op straat. Zo wordt Amsterdam schoner. Zelf ook scannen? Ga naar de installatiepagina.",
                sectionOneButton: "Setup",
                // Section 2
                sectionTwoSmallHeader: "Lokale overheden",
                sectionTwoHeader: "ODK in andere steden",
                sectionTwoText:
                    "ODK ook in jouw stad testen? Je kunt zelf de code implementeren. ODK is open source en de code is altijd up-to-date. Misschien kom je nog een bug tegen; laat dit vooral weten.",
                sectionTwoButton: "GitLab",
                // Section 3
                sectionThreeSmallHeader: "Burgers, studenten en onderzoekers",
                sectionThreeHeader: "Help met objecten herkennen",
                sectionThreeText:
                    "Om de nauwkeurigheid van de beeldherkenning te verbeteren, wordt het model continu getraind. Help door foto's van afval op straat te uploaden en de objecten te labelen.",
                sectionThreeButton: "Labeltool",
                // Section 4
                sectionFourSmallHeader: "Studenten, onderzoekers en lokale overheden",
                sectionFourHeader: "Andere objecten detecteren",
                sectionFourText:
                    "ODK herkent momenteel verschillende soorten afval. Wil je nieuwe objecten introduceren? Implementeer zelf de code via de Gitlab-pagina en gebruik de labeltool om ODK nieuwe objecten te leren herkennen.",
                sectionFourButtonOne: "Labeltool",
                sectionFourButtonTwo: "GitLab"
            }
        }
    }
}

export const aboutPageText = {
    i18n: {
        messages: {
            en: {
                title: "Meet the people behind",
                subTitle:
                    "We are enthusiastic members of municipalities and public organizations, innovating the way we look at our streets",
                peterDream: "Peter had a dream:",
                ourMission: "Our Mission",
                mission:
                    "“Having a real time image of the city’s condition that would enable us to keep the city clean, in a faster and more efficient way.”",
                missionBody:
                    "Interested people gathered to help Peter to make his dream come true. Now, we are a diverse team, from various parts of the Netherlands, bringing the best of image recognition to life and creating a better idea of what is happening in the city, in real time.",
                collaboratorsHeader: "Project members and collaborators"
            },
            nl: {
                title: "Ontmoet de mensen hierachter",
                subTitle:
                    "Wij zijn een groep enthousiaste werknemers van gemeenten en andere (semi-)overheidsorganisaties en kijken op een nieuwe manier naar het schoonhouden van de stad",
                ourMission: "Onze missie",
                peterDream: "Peter had een droom:",
                mission:
                    "“Een realtime beeld van afval op straat om onze stad sneller en efficiënter schoon te houden.”",
                missionBody:
                    "Uit verschillende hoeken zijn mensen samengekomen om Peter te helpen zijn droom waar te maken. Er staat nu een divers team van mensen uit verschillende windstreken. Door het gebruik van de laatste ontwikkelingen in beeldherkenning kan er een realtime beeld worden gevormd van de stad.",
                collaboratorsHeader: "Projectleden en samenwerkingspartners"
            }
        }
    }
}

export const roadmapPageText = {
    i18n: {
        messages: {
            en: {
                title: "Track the progress",
                subTitle:
                    "ODK is taking small steps into what can be a big change in the city",
                scrollPresent: "Scroll to current phase",
                link: "here"
            },
            nl: {
                title: "Volg de voortgang",
                subTitle:
                    "ODK zet kleine stappen voor grotere veranderingen in de stad",
                scrollPresent: "Scroll naar de huidige fase",
                link: "hier"
            }
        }
    }
}

export const roadmapCards = {
    i18n: {
        messages: {
            en: {
                roadmap: [{
                    dueDate: "2019-01-01",
                    title: "Low-budget object recognition",
                    text: "Gemeente Amsterdam, in collaboration with students of Hogeschool Windesheim in Almere, decides to invest into getting a better view of the state of the streets, by collecting real time data. The decision was made to use object recognition software to collect this data. It is developed within the municipality to increase ownership and reduce costs.",
                },
                {
                    dueDate: "2019-06-01",
                    title: "Garbage Detection model",
                    text: "Under the guidance of the lead AI developer Maarten Sukel, Gemeente Amsterdam wants to create its own machine learning model. The waste department shows increased interest in automated waste detection. This model used Faster-RCNN and was implemented in tensorflow. An initial annotated dataset of garbage was also shared. The source code can be found ",
                    video: "garbage-scan-tensorflow.mp4",
                    link: "https://github.com/maartensukel/garbage-object-detection-tensorflow"
                },
                {
                    dueDate: "2019-09-01",
                    title: "New team kick off",
                    text: "The project re-starts with new team members (project lead: Mark van der Net; data scientist: Maarten Sukel; developers: Stan Guldemond, Sven Brilleman, Yousef Kassem; designer: Cláudia Pinhão), a new name (Object Detection Kit) and new goals. The project’s scope, roles, approach and management methodology are defined.",
                },
                {
                    dueDate: "2019-09-15",
                    title: "Problem definition",
                    text: "The Amsterdam Waste Department will collaborate with ODK team. We are investigating whether garbage on the street can be reduced by using image recgnition. In this way, collecting garbage and finding a new purpose can be done easily.",
                },
                {
                    dueDate: "2019-09-30",
                    title: "New technical architecture",
                    text: "The new team redesigns and implements the software architecture. This architecture focuses on scaling up the amount of video streams to detect waste on the streets of Amsterdam.",
                },
                {
                    dueDate: "2019-10-01",
                    title: "Designing the Human Scan Car",
                    text: "ODK, in collaboration with AMS Institute and UNSENSE, join forces in a series of design sprints, where the benefits of image-based data collection on the streets, were debated. The need to create transparent, understandable and contestable processes in which citizens can participate, is identified. Easily identifying vehicles that scan objects and the data collected are among the proposed solutions.",
                },
                {
                    dueDate: "2019-10-15",
                    title: "Implementation of streaming app",
                    text: "The streaming app is developed into a PWA (Progressive Web App). This means that it is easily accessible by any device. The software is lightweight and behaves like a native app.",
                },
                {
                    dueDate: "2019-10-30",
                    title: "New version of Garbage Detection model",
                    text: "The new version of the ‘Garbage Detection model’ uses YOLOv3 and is implemented in PyTorch. This version is faster and has a higher accuracy. Also, a more balanced dataset is released. The source code can be found ",
                    video: "garbage-scan.mp4",
                    link: "https://github.com/maartensukel/yolov3-pytorch-garbage-detection/"
                },
                {
                    dueDate: "2019-11-01",
                    title: "Brand identity",
                    text: "Based on the mission to keep streets clean and repurpose garbage, ODK positions itself as an informal, versatile and high-tech platform. The inspiration of the design is the idea that ODK is always in progress and can be shaped to fit different needs. ODK wants to clarify and inspire the sensible use of AI on an urban level.",
                },
                {
                    dueDate: "2019-11-30",
                    title: "Expansion of the streaming data infrastructure",
                    text: "The technical architecture is expanded to connect the streams with new machine worker servers, which will be analyzing images. With these new machine worker servers, we're also giving researchers the possibility to train new data.",
                },
                {
                    dueDate: "2019-12-01",
                    title: "Litter Detection model in collaboration with other municipalities",
                    text: "Data scientists from the municipalities of Rotterdam, Utrecht, Oosterhout and Amsterdam work together during 'mini hackathons', tackling technical issues and expanding already existing models. For example, a first version of a litter detection model was created, capable of detecting cigarette butts. The goal of this collaboration is expanding knowledge quicker and combining research capabilities to increase the speed of innovation.",
                    video: "litter-scan.mp4"
                },
                {
                    dueDate: "2019-12-30",
                    title: "Update on Garbage Detection model",
                    text: "The Garbage Detection model is updated to increase performance by using new data collected by the streaming app. It is also combined with a filter that blurs vehicles and people in collected frames to protect citizens privacy.",
                    video: "garbage-scan.mp4"
                },
                {
                    dueDate: "2020-01-01",
                    title: "Image gathering for labeling",
                    text: "Images with different types of garbage from street views are collected throughout Amsterdam. These images provide valuable real data to train the ‘Garbage Detection model’.",
                },
                {
                    dueDate: "2020-01-30",
                    title: "Collaboration with 'AI for impact'",
                    text: "Gemeente Amsterdam, CBS and VNG are developing a strategic alliance for innovative policies, which aims to provide better public services throughout the Netherlands. It mainly focuses on fair algorithms, data science and AI. ODK integrates ‘AI for impact’ as one of the open source solutions.",
                },
                {
                    dueDate: "2020-02-01",
                    title: "First test",
                    text: "The first test is being performed with one car during one hour in the morning, under the supervision of ODK team members. The objective is to find potential errors in the system and to guarantee an optimal functionality of the app. Every test from now on will be used to improve the code.",
                },
                {
                    dueDate: "2020-03-04",
                    title: "Website launch",
                    text: "The website is part of the ODK communication strategy. It aims to inform municipalities how to join and to collaborate on new developments. The website also helps citizens to understand the need and benefits of image recognition in public spaces.",
                    photos: [
                        {
                            photo: "launch-1.jpg"
                        },
                        {
                            photo: "launch-2.jpg"
                        },
                        {
                            photo: "launch-3.jpg"
                        },
                        {
                            photo: "launch-4.jpg"
                        },
                        {
                            photo: "launch-5.jpg"
                        },
                        {
                            photo: "launch-6.jpg"
                        },
                        {
                            photo: "launch-7.jpg"
                        },
                        {
                            photo: "launch-8.jpg"
                        },
                    ]
                },
                {
                    dueDate: "2020-03-09",
                    title: "Start ODK Labeling Tool",
                    text: "Facing the lack of personalization of existing labeling tools, we decided to design a new one to train object detection models. With this tool, annotating is accessible for everyone who wants to contribute. Data scientists can also use this tool to gather data for their own purposes. With ODK Labeling Tool, other municipalities are also able to download models created by either us, other data scientists or other municipalities. Afterwards, the model can be easily implemented into their own ODK instance.",
                },
                {
                    dueDate: "2020-04-02",
                    title: "Public Space Design Sprint",
                    text: "In order to reduce wrongly disposed garbage in the city we need to have an objective image of what is happening on the streets, in real time. ODK can provide this image, however we need to act on it. To give a step further in this direction, we decided to organize a Design Sprint to look for effective ways to actively use ODK data. The different departments of public space are involved to give us insights on their current workflows and potential opportunities for ODK to be introduced with positive outcomes.",
                },
                // Start Deveopmentteam Public Space
                {
                    dueDate: "2020-06-01",
                    title: "Publish 'Urban Object Detection Kit' paper",
                    text: "In this paper, we propose Urban Object Detection Kit, a system for the real-time collection and analysis of street-level imagery. This paper shows how the Garbage Detection model functions and how it is implemented into ODK."
                },
                {
                    dueDate: "2020-06-06",
                    title: "Update on Garbage Detection model",
                    text: "The Garbage Detection model is updated to YOLOv4, the successor of YOLOv3. The model can now also recognize furniture, wood and mattresses."
                },
                {
                    dueDate: "2020-07-31",
                    title: "Second test",
                    text: "The second test aims to scale up to multiple cars. The objective is to find out if the server can handle multiple streams at the same time and to predict potential problems in future scale-ups. Also a purpose for this test is for users to experience the App and implement their feedback into improving the User Experience of the App.",
                },
                {
                    dueDate: "2020-09-15",
                    title: "Launch internal ODK Labeling Tool",
                    text: "The Labeling Tool is used by data scientitsts from the municipality of Amsterdam in order to create data that can be used to improve the Garbage Detection model.",
                },
                {
                    dueDate: "2020-09-30",
                    title: "Launch internal ODK Dashboard",
                    text: "The Dashboard is used by the waste department to visualize information we gather from the streets by scanning them.",
                },
                {
                    dueDate: "2020-10-15",
                    title: "Further testing and maintenance",
                    text: "The tests continue to scale up to more neighbourhoods. The software is continuously tested for privacy, performance and potential risks.",
                },
                {
                    dueDate: "2020-10-30",
                    title: "Documentation and open source",
                    text: "The ODK stack is deployed in a docker container, which makes it easy to use and deploy by other developers. The ODK version 1.0 is maintained and accessible on the GitLab Repository.",
                },
                // {
                //     dueDate: "2021-04-01",
                //     title: "Public dashboard to repurpose garbage",
                //     text: "A public dashboard is launched to be accessible by Amsterdam residents. It is possible to verify where certain types of garbage are located, in real time.",
                // },
                {
                    dueDate: "2021-06-01",
                    title: "Research possible applications",
                    text: "During 2020 the ODK-team collaborates in researches on possible applications of image recognition in public space - focused on public services.",
                },
                {
                    dueDate: "2021-08-30",
                    title: "New model created",
                    text: "The model used for the detection of urban issues is altered to another type of model with a higher accuracy and a more stable performance. We also change the output to align better with city services.",
                },
                {
                    dueDate: "2022-03-30",
                    title: "Implementation test",
                    text: "In Q4 of 2021 we will perform tests linking the Object Detection Kit to municipality system that plan the routes of our Garbage Trucks.",
                },
                {
                    dueDate: "2022-12-31",
                    title: "Using the collected insights for the responsible use of Artificial Intelligence in urban environments",
                    text: "We use the insights we gained from the Object Detection Kit for new projects,  where we focus on responsible uses of Artificial Intelligence. More information can be found ",
                    link: "http://www.amsterdamintelligence.com/"
                }],
            },
            nl: {
                roadmap: [{
                    dueDate: "2019-01-01",
                    title: "Low-budget objectherkenning",
                    text: "Gemeente Amsterdam wil, in samenwerking met studenten van Hogeschool Windesheim in Almere, een beter beeld krijgen van de staat van de openbare ruimte. Dit doet de gemeente door het verzamelen van realtime informatie met behulp van objectherkenning. Doordat de gemeente dit zelf ontwikkelt is het eigenaarschap groot en zijn de kosten relatief laag.",
                },
                {
                    dueDate: "2019-06-01",
                    title: "Afval detectie model",
                    text: "Gemeente Amsterdam besluit onder leiding van lead AI developer Maarten Sukel een eigen machine learning-model te maken. Collega’s die het vuil ophalen hebben steeds meer belangstelling in geautomatiseerde detectie van afval. Het model gebruikt Faster-RCNN en is geïmplementeerd in tensorflow. De gemeente deelt ook een eerste geannoteerde dataset met afval. De broncode vindt u ",
                    video: "garbage-scan-tensorflow.mp4",
                    link: "https://github.com/maartensukel/garbage-object-detection-tensorflow"
                },
                {
                    dueDate: "2019-09-01",
                    title: "Nieuw team begint",
                    text: "Het project maakt een nieuwe start met nieuwe teamleden (projectleider: Mark van der Net; datawetenschapper: Maarten Sukel; ontwikkelaars: Stan Guldemond, Sven Brilleman, Yousef Kassem; ontwerper: Cláudia Pinhão), een nieuwe naam (Object Detectie Kit) en nieuwe doelen. De scope, rollen, aanpak en manier van management worden gedefinieerd.",
                },
                {
                    dueDate: "2019-09-15",
                    title: "Probleemstelling",
                    text: "De gemeentelijke afdeling Afval & Grondstoffen gaat samenwerken met team ODK. Er wordt onderzocht of met behulp van ODK  het afval op straat kan worden verminderd door afval sneller op te sporen, waardoor inzameling en herbestemming eenvoudiger worden.",
                },
                {
                    dueDate: "2019-09-30",
                    title: "Nieuwe technische architectuur",
                    text: "Het nieuwe team ontwerpt en implementeert de software-architectuur opnieuw. Deze architectuur is gericht op het verhogen van het aantal videostreams om afval te detecteren.",
                },
                {
                    dueDate: "2019-10-01",
                    title: "Het ontwerpen van de scanauto",
                    text: "ODK, AMS Institute en UNSENSE bundelen hun krachten in een reeks ontwerpsprints, waarbij de voor- en nadelen van het verzamelen van beelden op straat worden besproken. De deelnemers benoemen de noodzaak om transparante, begrijpelijke en betwistbare processen te creëren waaraan burgers kunnen deelnemen. Het gemakkelijk herkennen van voertuigen die objecten scannen en het kunnen inzien van verzamelde gegevens zijn twee van de voorgestelde oplossingen.",
                },
                {
                    dueDate: "2019-10-15",
                    title: "Implementatie van streaming-app",
                    text: "De streaming-app is ontwikkeld tot een PWA (Progressive Web App). Dit betekent dat deze gemakkelijk toegankelijk is voor elk apparaat. De software is lichtgewicht en gedraagt zich als een normale app.",
                },
                {
                    dueDate: "2019-10-30",
                    title: "Nieuwe versie van het afval detectie model",
                    text: "De nieuwe versie van het 'Garbage Detection-model' maakt gebruik van YOLOv3 en is geïmplementeerd in PyTorch. Deze versie is sneller en heeft een hogere nauwkeurigheid. Ook is er een meer gebalanceerde dataset uitgebracht. De broncode vindt u ",
                    video: "garbage-scan.mp4",
                    link: "https://github.com/maartensukel/yolov3-pytorch-garbage-detection/"
                },
                {
                    dueDate: "2019-11-01",
                    title: "Merkidentiteit",
                    text: "Op basis van de missie om straten schoon te houden en afval te hergebruiken, positioneert ODK zich als een informeel, veelzijdig en hightech platform. De inspiratie van het design laat zien dat ODK altijd in ontwikkeling is en dat het omgevormd kan worden naar behoefte. ODK wil inspireren en laten zien hoe een stad AI slim kan inzetten.",
                },
                {
                    dueDate: "2019-11-30",
                    title: "Uitbreiding van de streaming data-infrastructuur",
                    text: "De technische architectuur is uitgebreid om de streams te verbinden met nieuwe machine worker servers die afbeeldingen analyseren. Met deze nieuwe machine worker servers geven we onderzoekers ook de mogelijkheid om het model te laten leren op basis van nieuwe gegevens.",
                },
                {
                    dueDate: "2019-12-01",
                    title: "Zwerfafval detectie model in samenwerking met andere gemeenten",
                    text: "Datawetenschappers van een viertal Nederlandse gemeenten (Rotterdam, Utrecht, Oosterhout en Amsterdam) werken samen in  'mini-hackathons' om nieuwe technieken te ontwikkelen. Bij de eerste mini-hackathon in december 2019 is er bijvoorbeeld een model gemaakt dat sigarettenpeuken op straat snel kan herkennen en tellen. Het doel van deze samenwerking is het uitbreiden van kennis en het sneller kunnen toepassen van innovaties.",
                    video: "litter-scan.mp4"
                },
                {
                    dueDate: "2019-12-30",
                    title: "Update van het afval detectie model",
                    text: "Het afval detectie model is geüpdatet en behaalt nu betere resultaten door het gebruik van nieuwe gegevens die zijn verzameld door de streaming-app. Ook is er in het model een filter toegevoegd dat auto’s en mensen onherkenbaar maakt op de verzamelde beelden, om zo de privacy van bewoners te beschermen.",
                    video: "garbage-scan.mp4"
                },
                {
                    dueDate: "2020-01-01",
                    title: "Afbeeldingen verzamelen voor betere herkenning",
                    text: "We verzamelen afbeeldingen met verschillende soorten afval door heel Amsterdam. Met deze afbeeldingen kunnen we het ‘Garbage Detection model’ trainen om beter te worden in het herkennen van het afval.",
                },
                {
                    dueDate: "2020-01-30",
                    title: "Samenwerking met 'AI voor impact'",
                    text: "Gemeente Amsterdam ontwikkelt in samenwerking met CBS en VNG een strategische alliantie voor innovatief beleid, gericht op betere publieke dienstverlening in heel Nederland. De alliantie richt zich vooral op eerlijke algoritmen, datawetenschap en AI. ODK integreert ‘AI voor impact’ als een van de open source-oplossingen.",
                },
                {
                    dueDate: "2020-02-01",
                    title: "Eerste test",
                    text: "Onder toezicht van het ODK-team wordt de eerste test uitgevoerd met één auto voor één uur in de ochtend. Het doel is om eventuele fouten in het systeem te vinden en een optimale functionaliteit van de app te garanderen. Vanaf nu wordt elke test gebruikt om de code te verbeteren.",
                },
                {
                    dueDate: "2020-03-04",
                    title: "Websitelancering",
                    text: "De website maakt deel uit van de ODK-communicatiestrategie. De website laat gemeenten zien hoe ze zich kunnen aansluiten bij nieuwe ontwikkelingen en hoe ze met ons kunnen samenwerken. De website informeert daarnaast bewoners over de voordelen van beeldherkenning in de openbare ruimte.",
                    photos: [
                        {
                            photo: "launch-1.jpg"
                        },
                        {
                            photo: "launch-2.jpg"
                        },
                        {
                            photo: "launch-3.jpg"
                        },
                        {
                            photo: "launch-4.jpg"
                        },
                        {
                            photo: "launch-5.jpg"
                        },
                        {
                            photo: "launch-6.jpg"
                        },
                        {
                            photo: "launch-7.jpg"
                        },
                        {
                            photo: "launch-8.jpg"
                        },
                    ]
                },
                {
                    dueDate: "2020-03-09",
                    title: "Start ODK Labeling Tool",
                    text: "Wegens het gebrek aan personalisatie van bestaande labeling tools besluiten wij om een nieuwe tool te creëren om het object detectie model te trainen. Met deze tool wordt het mogelijk voor iedereen om bij te dragen met de nodige annotaties. Data scientists kunnen deze tool gebruiken om data te genereren voor eigen gebruik. Met de Labeling Tool kunnen andere gemeenten modellen downloaden die gemaakt zijn door ons, of door andere data scientists of gemeenten. Daarna kan het model gemakkelijk worden geïmplementeerd in een eigen instantie van ODK.",
                },
                {
                    dueDate: "2020-04-02",
                    title: "Design Sprint Openbare Ruimte",
                    text: "Voor de aanpak van verkeerd aangeboden afval in de stad is er behoefte aan een objectief beeld van de straat. Met ODK kan dit beeld worden gegeven. Daarom is besloten een Design Sprint te organiseren. Gedurende deze week wordt er gekeken hoe er actief gebruik kan worden gemaakt van ODK en hoe de interactie met de object-data eruit kan zien. Met deze bijeenkomst van afdelingen van openbare ruimte willen we inzicht geven in objectieve informatie over de straten van Amsterdam.",
                },
                // Start Deveopmentteam Public Space
                {
                    dueDate: "2020-06-01",
                    title: "Publicatie van de paper 'Urban Object Detection Kit'",
                    text: "Deze paper gaat over de Urban Object Detection Kit, een systeem voor het real-time verzamelen van data en analyse van street-level imagery. In deze paper wordt duidelijk gemaakt hoe het afval detectie model functioneert en hoe deze is geïmplementeerd in ODK.",
                },
                {
                    dueDate: "2020-06-06",
                    title: "Update van het afval detectie model",
                    text: "Het afval detectie moodel is geüpdatet naar YOLOv4, de opvolger van YOLOv3. Het model herkent nu ook meubels, hout en matrassen.",
                },
                {
                    dueDate: "2020-07-31",
                    title: "Tweede test",
                    text: "Het doel van de tweede test is te achterhalen of de server meerdere streams tegelijkertijd kan verwerken en om potentiële problemen bij toekomstige schaalvergroting te voorspellen. Ook wordt er gekeken naar hoe de gebruikers de App ervaren. Met hun feedback kan de User Experience van de App worden verbeterd.",
                },
                {
                    dueDate: "2020-09-15",
                    title: "Interne lancering ODK Labeling Tool",
                    text: "De Labeling Tool wordt gebruikt door data scientists van de Gemeente Amsterdam. De tool zorgt voor het kunnen creëren van data dat gebruikt kan worden om het afval detectie model mee te verbeteren.",
                },
                {
                    dueDate: "2020-09-30",
                    title: "Interne lancering ODK Dashboard",
                    text: "Het Dashboard wordt gebuikt door de afvalafdeling van de Gemeente Amsterdam om een visualisatie te vormen van de data die wordt opgehaald bij het scannen van de straat.",
                },
                {
                    dueDate: "2020-10-15",
                    title: "Verdere tests en onderhoud",
                    text: "We schalen de tests op naar andere buurten. De software wordt continu getest op privacy, prestaties en potentiële risico's.",
                },
                {
                    dueDate: "2020-10-30",
                    title: "Documentatie en open source",
                    text: "De ODK-stack is geplaatst in een docker-container, waardoor deze eenvoudig door andere ontwikkelaars kan worden gebruikt. De ODK-versie 1.0 wordt onderhouden en is toegankelijk via de GitLab Repository.",
                },
                // {
                //     dueDate: "2021-04-01",
                //     title: "Openbaar dashboard om afval te hergebruiken",
                //     text: "Een openbaar dashboard wordt gelanceerd en is toegankelijk voor Amsterdammers. Het is mogelijk om realtime te controleren waar bepaalde soorten afval zich bevinden.",
                // },
                {
                    dueDate: "2021-06-01",
                    title: "Onderzoek mogelijke toepassingen",
                    text: "In 2020 werkt het ODK-team mee aan onderzoek naar mogelijke toepassingen van beeldherkenning in de openbare ruimte - gericht op openbare diensten.",
                },
                {
                    dueDate: "2021-08-30",
                    title: "Nieuw model gemaakt",
                    text: "Het model voor detectie van problemen in de openbare ruimte is aangepast. Het is nu nauwkeuriger en de prestaties zijn verbeterd en stabieler. Ook is de output van de data meer in lijn gebracht met de gemeentelijke dienstverlening.",
                },
                {
                    dueDate: "2022-03-30",
                    title: "Implementatie test",
                    text: "In Q4 van 2021 zullen we beginnen met het testen van een connectie tussen Object Detection Kit en het routeplanningssysteem van de gemeente, waarin routes voor vuilniswagens worden aangemaakt.",
                },
                {
                    dueDate: "2022-12-31",
                    title: "De verzamelde inzichten gebruiken voor verantwoord gebruik van kunstmatige intelligentie in de openbare ruimte",
                    text: "De inzichten die we hebben opgedaan met de Object Detection Kit gebruiken we voor nieuwe projecten, waarbij we ons richten op verantwoord gebruik van kunstmatige intelligentie. Meer informatie vindt u ",
                    link: "http://www.amsterdamintelligence.com/"
                }]
            }
        }
    }
}

export const faqPageText = {
    i18n: {
        messages: {
            en: {
                title: "Frequently Asked Questions",
                subTitle:
                    "We've tried to anticipate your questions and answer them beforehand. But if you still have a question, feel free to contact us",
                sectionOneTitle: "General Questions",
                sectionTwoTitle: "Citizens' Questions",
                sectionThreeTitle: "Technical Questions",
                link: "here"
            },
            nl: {
                title: "Veelgestelde vragen (FAQ)",
                subTitle:
                    "We hebben geprobeerd om op verschillende vragen te anticiperen en deze vooraf te beantwoorden. Maar als u nog steeds een vraag heeft, neem dan gerust contact met ons op",
                sectionOneTitle: "Algemene Vragen",
                sectionTwoTitle: "Vragen van burgers",
                sectionThreeTitle: "Technische Vragen",
                link: "hier"
            }
        }
    }
}

export const faqMessages = {
    i18n: {
        messages: {
            en: {
                collapses: [{
                    title: "What is ODK?",
                    text: "Object Detection Kit is an open source platform for municipalities, initiated by Gemeente Amsterdam. We want to increase the efficiency of urban services by automatically detecting garbage on the streets and communicate this information to the responsible services."
                },
                {
                    title: "Which problem is ODK trying to solve?",
                    text: "Nowadays, anyone can see different types of garbage sitting along the streets of Amsterdam, especially near underground waste containers. In any case, the objective of the municipality of Amsterdam is to keep the streets as clean as possible by using the forces of the waste department efficiently. This solution monitors the condition of the streets by spotting garbage items and to determine which interventions are necessary in order to collect waste quickly and keep the streets clean."
                },
                {
                    title: "Which are the benefits for citizens?",
                    text: "Ultimately ODK should help reduce the nuisance of garbage in the streets. Citizens will have to report garbage less often, since automatic reports will speed up the process."
                },
                {
                    title: "Why do we need scanners if we have people?",
                    text: "People working on the streets have daily scheduled activities, within their expertise and department. Reporting garbage from other departments is an extra activity that sometimes interferes with the quality of their main job. Having automated reports reduces the workload and guarantees that each detection results in a single report."
                },
                {
                    title: "Why do we use scanners on municipality vehicles?",
                    text: "Gemeente Amsterdam has hundreds of vehicles on the streets every day, keeping the city cleanand safe. Early in the morning cleaning vehicles have already covered most of the city. By letting them scan regularly, garbage in the city can be mapped quickly."
                }
                ],

            },
            nl: {
                collapses: [{
                    title: "Wat is het doel van ODK?",
                    text: "Object Detection Kit is een open source platform voor gemeenten, op initiatief van gemeente Amsterdam. Door afval op straat automatisch te detecteren en deze informatie door te geven aan de verantwoordelijke dienst, kan afval ophalen efficiënter worden ingericht."
                },
                {
                    title: "Welk probleem probeert ODK op te lossen?",
                    text: "Op sommige plekken in Amsterdam ligt veel afval op straat, vooral in de buurt van ondergrondse afvalcontainers. Gemeente Amsterdam wil de straten zo schoon mogelijk houden en haar afvaldienst zo efficiënt mogelijk inzetten. Met ODK wordt afval op straat gedetecteerd en kunnen we beter bepalen welke interventies nodig zijn om de straat (snel) schoon te krijgen en te houden."
                },
                {
                    title: "Wat zijn de voordelen voor de burgers?",
                    text: "Met ODK kan de overlast van afval op straat verminderd worden. Bewoners hoeven door de automatische rapporten minder vaak verkeerd geplaatst afval te melden."
                },
                {
                    title: "Waarom scanners gebruiken in plaats van mensen?",
                    text: "De medewerkers die onze straten schoonmaken zitten vast aan een planning en werken binnenhun eigen expertise en afdeling. Het melden van afval aan andere afdelingen is extra werk dat soms ten koste gaat van hun hoofdtaken. Door automatische rapporten wordt de werkdruk verminderd en komt de melding snel bij de juiste afvaldienst terecht."
                },
                {
                    title: "Waarom gebruiken we scanners op gemeente voertuigen?",
                    text: "Gemeente Amsterdam heeft elke dag honderden voertuigen op straat om de stad schoon en veilig te houden. Veegwagens hebben al vroeg in de ochtend de hele stad gezien. Door verschillende wagens regelmatig te laten scannen kunnen ze dus snel de hele stad in beeld brengen."
                }
                ],

            },


        }

    }
}

export const faqMessagesTwo = {
    i18n: {
        messages: {
            en: {
                collapsesTwo: [{
                    title: "Why do garbage vehicles need to scan instead of collect?",
                    text: "The garbage vehicles do collect too, of course! However, different garbage vehicles collect different types of garbage. For example, if a paper collection vehicle detects cardboard on the street, this cardboard will be immediately collected. However, if the same vehicle detects furniture, it will have to report to the correspondent services, to be picked up. In this way, it is easier to communicate garbage locations between services."
                },
                {
                    title: "How do I know if a car is scanning, and what it can detect?",
                    text: "Vehicles from Gemeente Amsterdam, that scan garbage will have a sticker identifying what typeof objects are being scanned. The type of objects will be represented by a symbol. We don’t save any sensitive data like faces or number plates."
                },
                {
                    title: "Are garbage cars scanning everywhere, at any time?",
                    text: "No. At the moment, only a few vehicles in a few selected areas will scan garbage."
                },
                {
                    title: "Are scanners saving images?",
                    text: "No sensitive data, like faces and number plates, will be stored. Once an object is detected it is converted into data (quantity, time and location). For development purposes some images will be stored temporarily, sensitive data will be blurred."
                },
                {
                    title: "Are scanners only detecting garbage?",
                    text: "For now, they are. Other objects can be detected if there is a reasonable demand. When this happens, it will be communicated on the vehicle. We will never detect and/or store images with privacy sensitive information, like faces or license plates (neither for development purposes)."
                }
                ],

            },
            nl: {
                collapsesTwo: [{
                    title: "Kunnen vuilniswagens het gescande afval niet meteen meenemen?",
                    text: "De scannende vuilniswagens verzamelen natuurlijk ook afval! Alleen verzamelen verschillende vuilniswagens verschillende soorten afval.Als een vuilniswagen voor papier op zijn rit karton op straat ziet liggen, wordt dit meteen meegenomen. Wanneer dezelfde vuilniswagen echter meubels tegenkomt, moet een andere afvaldienst het ophalen. Door te scannen wordt het gedetecteerde afval en de locatie direct doorgegeven aan de juiste afdeling. "
                },
                {
                    title: "Hoe weet ik wanneer een auto scant en welke objecten er worden herkend?",
                    text: "De voertuigen van Gemeente Amsterdam die afval scannen, krijgen een sticker. Hierop staat met symbolen aangegeven welk soort objecten ze kunnen scannen. Er worden geen gevoelige gegevens opgeslagen, zoals gezichten en kentekens."
                },
                {
                    title: "Scannen de voertuigen overal en altijd?",
                    text: "Nee. Op dit moment scannen we slechts met een aantal voertuigen op een aantal geselecteerde locaties naar afval. Ook in de toekomst is continu en overal scannen niet aan de orde. "
                },
                {
                    title: "Worden de gescande beelden opgeslagen?",
                    text: "Er worden geen gevoelige gegevens, zoals gezichten en kentekens, opgeslagen. Zodra een object is gedetecteerd, wordt het beeld omgezet in gegevens (hoeveelheid, tijd en locatie). Voor ontwikkelingsdoeleinden worden sommige afbeeldingen tijdelijk opgeslagen, gevoelige gegevens worden hierop onherkenbaar gemaakt. "
                },
                {
                    title: "Detecteren scanners alleen maar afval?",
                    text: "Op dit moment wel. Als er veel vraag naar een ander object ontstaat, zou dit kunnen veranderen. Wanneer dit gebeurt, wordt dit op het voertuig vermeld. ODK zal nooit privacygevoelige informatie opslaan, zoals gezichten of kentekens. Ook niet voor ontwikkelingsdoeleinden."
                }
                ],

            },


        }

    }
}


export const faqMessagesThree = {
    i18n: {
        messages: {
            en: {
                collapsesThree: [{
                    title: "How do I know if a municipality vehicle is scanning right now?",
                    text: "Scanning vehicles will have a sticker indicating what they are scanning, but this will not yet show if the scanner is active. In the future we want to implement visible design changes, which will show if a vehicle is scanning or not."
                },
                {
                    title: "Are scanners recording me?",
                    text: "No sensitive data, like faces and license plates, will be stored. Once a garbage related object is detected, it is converted into data (quantity, time and location). For development purposes some images will be stored temporarily, sensitive data will be blurred."
                },
                {
                    title: "What about privacy?",
                    text: "We adhere to the personal data framework of the Municipality of Amsterdam, but we are still working on a specific privacy policy for this project. You can find more about the personal data framework ",
                    link: "https://www.amsterdam.nl/privacy/privacyverklaring/"
                }
                ],

            },
            nl: {
                collapsesThree: [{
                    title: "Hoe weet ik of een gemeente voertuig op dit moment aan het scannen is?",
                    text: "Voertuigen hebben een sticker waarop staat wat ze kunnen scannen, maar dit geeft nog niet aan of de scanner actief is. In de toekomst willen we zichtbaar maken of een scanner actief is of niet."
                },
                {
                    title: "Word ik opgenomen door scanners?",
                    text: "Er worden geen gevoelige gegevens, zoals gezichten en kentekens, opgeslagen. Zodra een object is gedetecteerd, wordt het beeld omgezet in gegevens (hoeveelheid, tijd en locatie). Voor ontwikkelingsdoeleinden worden sommige afbeeldingen tijdelijk opgeslagen, gevoelige gegevens worden hierop onherkenbaar gemaakt."
                },
                {
                    title: "Hoe zit het met privacy?",
                    text: "We houden ons aan het stedelijk kader persoonsgegevens van de Gemeente Amsterdam. We werken nog aan een specifiek privacybeleid voor dit project. Meer informatie over het stedelijk kader persoonsgegevens vindt u ",
                    link: "https://www.amsterdam.nl/privacy/privacyverklaring/"
                }
                ],

            },


        }

    }
}

export const faqMessagesFour = {
    i18n: {
        messages: {
            en: {
                collapsesFour: [{
                    title: "How can I give my opinion as a resident?",
                    text: "We are aware that there is a public debate around the collection of data in the public space. We intend to collaborate with neighbourhoods to discuss test schedules and future improvements. We try to do our work as transparent and collaborative as possible, by developing our software open source and by keeping you updated on this website. In any case, if you have any questions or if you want to give your opinion, please send us an email."
                },
                {
                    title: "When can I expect to see cars scanning in the city?",
                    text: "The first tests are performed at locations with garbage in a neighbourhood, in Amsterdam, in the beginning of 2020. After these tests we will evaluate which other neighbourhoods have potential for an iteration of testing. For more information see the roadmap of this website."
                }
                ],

            },
            nl: {
                collapsesFour: [{
                    title: "Hoe kan ik mijn stem laten horen als een inwoner?",
                    text: "We zijn ons ervan bewust dat er een discussie bestaat over het verzamelen van gegevens in de openbare ruimte. We willen samen werken met buurten om testschema's en toekomstige verbeteringen te bespreken. We proberen ons werk zo transparant mogelijk te doen, door onze software open source te ontwikkelen en bewoners op de hoogte te houden op deze website. Stuur ons een e-mail als u vragen heeft of als u uw mening wilt geven."
                },
                {
                    title: "Wanneer kan ik scannende voertuigen in de stad verwachten?",
                    text: "De eerste testen zijn begin 2020 uitgevoerd in een buurt in Amsterdam. Na deze testen wordt gekeken welke andere buurten geschikt zijn. Zie de roadmap-pagina van deze website voor meer informatie."
                }
                ],

            },


        }

    }
}

export const faqMessagesFive = {
    i18n: {
        messages: {
            en: {
                collapsesFive: [{
                    title: "Can I access the real time location of garbage?",
                    text: "For now, only municipalities will have access to the dashboard with real time information. However, a public version is planned for citizens."
                },
                {
                    title: "What model is being used to detect garbage?",
                    text: "For the detection the yolov3 model trained on annotated images of garbage is used. This model is chosen because it has a fast performance and accurate detections. The source code and more information can be found ",
                    link: "https://github.com/maartensukel/yolov3-pytorch-garbage-detection"
                }
                ],


            },
            nl: {
                collapsesFive: [{
                    title: "Kan ik toegang krijgen tot de real time locaties van afval?",
                    text: "Voorlopig hebben alleen gemeenten toegang tot het dashboard met realtime informatie. Er staat een openbare versie gepland voor bewoners. "
                },
                {
                    title: "Welke model wordt gebruikt voor het detecteren van afval?",
                    text: "Voor de detectie wordt het yolov3-model gebruikt, dat speciaal is getraind op het herkennen van afbeeldingen van afval. Dit model is snel een nauwkeurig. De broncode en meer informatie vindt u ",
                    link: "https://github.com/maartensukel/yolov3-pytorch-garbage-detection"
                }
                ],


            },


        }

    }
}

export const faqMessagesSix = {
    i18n: {
        messages: {
            en: {
                collapsesSix: [{
                    title: "Can I get access to historical data?",
                    text: "Firstly processed data will be available to the municipality. In the future, historical data will be available through open data platforms."
                },
                {
                    title: "Is there an API?",
                    text: "We are working together with the VNG to make relevant data accessible through the NLX platform. Also, we intend to make our machine learning capabilities accessible through this network, so we can collaborate on training models together with other municipalities and universities."
                }
                ]


            },
            nl: {
                collapsesSix: [{
                    title: "Kan ik toegang krijgen tot historische data?",
                    text: "Op het moment zijn de verwerkte gegevens, zoals afvaltellingen en bijbehorende locaties, alleen beschikbaar voor de gemeente. In de toekomst zullen historische gegevens beschikbaar komen via open dataplatforms."
                },
                {
                    title: "Is er een API beschikbaar?",
                    text: "We werken samen met de VNG om relevante gegevens toegankelijk te maken via het NLX-platform. Daarnaast willen we onze machine learning capaciteit toegankelijk maken via dit netwerk, zodat we kunnen samenwerken met andere gemeenten en universiteiten in het trainen van modellen."
                }
                ]
            },
        }

    }
}

export const contactText = {
    i18n: {
        messages: {
            en: {
                contactUs: "Contact us",
                required: "required",
                fieldRequired: "This field is required",
                emailRequired: "This field should contain a valid email",
                message: "Message",
                body: "We will do our best to answer your question as soon as possible",
                messageSent: "Message sent",
                messageNotSent: "Message was not sent. Please try again"
            },
            nl: {
                contactUs: "Neem contact op",
                required: "verplicht",
                fieldRequired: "Dit veld is verplicht",
                emailRequired: "Dit veld moet een geldig email bevatten",
                message: "Bericht",
                body:
                    "We zullen ons best doen om uw vraag zo snel mogelijk te beantwoorden",
                messageSent: "Bericht gestuurd",
                messageNotSent: "Bericht is niet verzonden. Probeer opnieuw"
            }
        }
    }
}